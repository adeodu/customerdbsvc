import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent,
        redirectTo: '',
        pathMatch: 'full'
      },
      {
        path: '404',
        component: NotfoundComponent
      }
    ])
  ],
  // providers: [{provide: APP_BASE_HREF, useValue : '' }],
  providers: [{provide: APP_BASE_HREF, useValue : '/msportal/customerdb' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
