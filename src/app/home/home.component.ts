import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private title: Title,
    private router: Router ) {
    title.setTitle('CustomerDB UI Service');
  }

  isAuthenticated: boolean = false;

  ngOnInit() {
    if(!this.isAuthenticated) {
      if(window.sessionStorage.auth_key_homepage == null) {
        this.router.navigateByUrl('/404');
      } else {
        // stay on landing page for CustomerDB Ui service.
        this.isAuthenticated = true;
      }
    }
  }

}
